"use strict";
var app = getApp();
Page({
    data: {
        //消息
        msgcount: 3,       
        gate: {
            img:"icon-return-home.png",
            name: "选择场所"
        },
        //区域列表
        list: [],
    },
    //获取区域
    list: function () {
        var list = [{
            id: 1,
            name: "会客区",
            img:'icon-keting.png',   
            //正在打开的灯数量         
            switchCount: 9,
            //正在开启的空调数量
            airCount: 0,
            //正在打开的窗帘数量
            windowCount: 5,                              
        }, {
            id: 2,
            img:"icon-dianying.png",
            name: "影院区",          
            switchCount: 2,
            airCount: 1,
            windowCount: 2,                 
        }]

        this.setData({
             list
        })
    },
    onLoad: function () {},
    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        this.list();
    },

    onShow: function () {
        var _this = this;
        var token = wx.getStorageSync('token')
        if (token == "") {
            wx.navigateTo({
                url: '../login/login',
            })
            return;
        }
        var gate = wx.getStorageSync('gate')   
        if (gate != "") {
            this.setData({
                gate: gate
            })
        }
        if (gate == '') {
            wx.showModal({
                title: "切换场所",
                success: function (res) {
                    if (res.cancel) {
                        //点击取消,默认隐藏弹框
                    } else {
                        wx.navigateTo({
                            url: '../gate/manage/manage',
                        })
                    }
                },
            })
            return;
        };

        wx.setNavigationBarTitle({
          title: gate.name,
        })
        this.list();
        this.msgCount();

    },
    //获取未读消息
    msgCount: function () {
      
    },
    onPullDownRefresh: function () {
        this.list();
        this.msgCount();
        wx.stopPullDownRefresh();
    },
    areaClick: function (e) {
        var id = e.currentTarget.dataset.id
        var name = e.currentTarget.dataset.name;
        wx.navigateTo({
            url: '../area/area?id=' + id + "&name=" + name,
        })
    },
    changsuo: function () {
        wx.navigateTo({
            url: '../changsuo/manage/manage',
        })
    },
    cjkj: function () {
        wx.navigateTo({
            url: '../scene/kj/kj',
        })
    },
    sbkj: function () {
        wx.navigateTo({
            url: '../device/sbkj/sbkj',
        })
    },
    safe: function () {
        wx.navigateTo({
            url: '../safe/safe',
        })
    },
    logs: function () {
        wx.navigateTo({
            url: '../logs/logs',
        })
    }
});