var smh = require("../../../utils/smh.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    on: ["关", "开"],
    mode: ["模式", "制冷", "制热", "抽湿", "送风", "自动"],
    wind: ["风速", "小风", "中风", "大风"]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var id = options.id;
    this.get(id)
  },
  get: function (id) {
    var that = this
    smh.get("device?id=" + id, {
      success: function (res) {
        that.set(res.data)
      },
      fail: function (res) {
        wx.showToast({
          title: res.msg,
          icon: "loading"
        })
      }
    })
  },
  set: function (dev) {
    var that = this;
    wx.setNavigationBarTitle({
      title: dev.displayName,
    })

    var data = smh.toHex(dev.param)
    //开关位4
    var mode = data[4]

    if (mode == 0 || mode == 6) {
    } else {
      dev.mode = that.data.mode[mode]
    }
    var w = parseInt(data[7]);
    if ((w > 0) && (w < 4)) {
      dev.wind = that.data.wind[w];
    }

    var tem = parseInt(data[6], 16);
    if (tem > 50) {
      tem = 26;
    }
    dev.tem = tem
    var set = parseInt(data[5], 16);
    if (set > 50) set = 25; //设定温度
    dev.set = set;

    that.setData({
      device: dev
    })
  },
  getdata: function () {
    var p = smh.toHex(this.data.device.param);
    var data = [];
    //模式
    data.push(p[4]);
    //设定温度
    data.push(p[5]);
    //占位
    data.push("00");
    data.push("00");
    //风速
    data.push(p[7]);

    return data;
  },
  open: function () {
    var that = this;
    var dev = that.data.device;
    var param = this.getdata()
    param[0] = dev.on? "06" : "07";
    param[1] = "00"
    param[4] = "00"
    var data = param.join("");

    smh.opt(dev.id, "14", 3, data, {
      success: function (res) {
        if (param[0] == "06") {
          dev.on = false;
        } else {
          dev.on = true
        }
        that.setData({
          device: dev
        })
      },
      fail: function (res) {
        wx.showToast({
          title: res.msg,
          icon: "loading"
        })
      }
    })
  },
  setmode: function (e) {
    var that = this;
    var mode = e.currentTarget.dataset.mode;
    var dev = this.data.device;
    var param = this.getdata();

    param[0] = "0" + mode;
    param[1] = "00"; //温度  181016+
    param[4] = "00"; //风速置 181016+

    var data = param.join("");
    smh.opt(dev.id, "14", 3, data, {
      success: function (res) {
        dev.on = true
        dev.mode = that.data.mode[mode]
        that.setData({
          device: dev
        })
      },
      fail: function (res) {
        wx.showToast({
          title: res.msg,
          icon: "loading"
        })
      }
    })
  },
  setwind: function (e) {
    var wind = e.currentTarget.dataset.mode;
    var that = this
    var dev = this.data.device
    var param = this.getdata();
    if((param[0]=="06")||(param[0]=="FF"))
				param[0]="00";//模式  190626+
    param[4] = "0" + wind; //	param[0]="00";//模式   181016+
    param[1] = "00"; //温度 181016+
    var data = param.join("");
    smh.opt(dev.id, "14", 3, data, {
      success: function (res) {
        dev.on = true
        dev.wind = that.data.wind[wind]
        that.setData({
          device: dev
        })
      },
      fail: function (res) {
        wx.showToast({
          title: res.msg,
          icon: "loading"
        })
      }
    })
  },
  wenduadd:  function (e) {
    var tem = parseInt(e.currentTarget.dataset.mode);
    var that = this
    var dev = this.data.device
    var param = this.getdata();
    if((param[0]=="06")||(param[0]=="FF"))
        param[0]="00";

    var w = parseInt(dev.set) + tem;//
    if(w < 16) w = 16;//
    if(w > 30) w = 30;//
    param[1] = w.toString(16);//温度  //
    param[4]="00";//风速置  181016+
    var data = param.join("").toUpperCase();
    smh.opt(dev.id, "14", 3, data, {
      success: function (res) {
        dev.set =w
        dev.on = true
        that.setData({
          device: dev
        })
      },
      fail: function (res) {
        wx.showToast({
          title: res.msg,
          icon: "loading"
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
     var id=this.data.device.id

     this.get(id);
     wx.stopPullDownRefresh({
       complete: (res) => {},
     })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})