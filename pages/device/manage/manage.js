var smh = require("../../../utils/smh.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageSize: 20,
    currentTab: 0,
    winWidth: 0,
    winHeight: 0,
    scrollleft: 0,
    moreshow: "more-hidden",
    action: false,
    newlist: [],
    areaList: [],
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var action = options.action;
    console.log(action);
    this.setData({
      action: action
    })
    var that = this;
    /** 
     * 获取系统信息 
     */
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight
        });
      }
    });
    this.list();

  },
  updata: function () {
    //  this.list();
    if (this.data.currentTab == 0) {
      this.all()
    } else {
      this.areaSclist()
    }
  },
  list: function () {
    var list = [{
      id: 1,
      name: "客厅",
      devices: [{
        id: 1,
        type: '0101',
        name: "开关",
        on: false
      },
      {
        id: 1,
        type: '0101',
        name: "开关",
        on: true
      }
      ]
    },
    {
      id: 1,
      name: "健身房",
      devices: [{
        id: 1,
        type: '0101',
        name: "开关",
        on: false
      },
      {
        id: 1,
        type: '0101',
        name: "开关",
        on: true
      }
      ]
    },
    {
      id: 1,
      name: "楼顶花园",
      devices: [{
        id: 1,
        type: '0101',
        name: "开关",
        on: false
      },
      {
        id: 1,
        type: '0101',
        name: "开关",
        on: true
      }
      ]
    }
    ]
    this.setData({
      areaList: list
    })
  },
  all: function () {
    var list = [{
      id: 1,
      name: "开关",
      type: '0101',
      on: true,
      data: '01'
    },
    {
      id: 2,
      name: "调光",
      type: '0102',
      on: true,
      data: '51'
    },
    {
      id: 3,
      name: "插座",
      type: '0103',
      on: true,
      data: '01'
    },
    {
      id: 4,
      name: "窗帘",
      type: '0201',
      on: false,
      data: '01'
    },
    {
      id: 6,
      name: "空调",
      type: '0401',
      on: true,
      data: '01'
    },
    {
      id: 6,
      name: "电视",
      type: '0402',
      on: false,
      data: '01'
    },
    {
      id: 6,
      name: "背景音乐",
      type: '0405',
      on: false,
      data: '01'
    },
    {
      id: 1,
      name: "电表",
      type: '0301',
      on: true,
      data: '01'
    },
    {
      id: 1,
      name: "红外探测",
      type: '0503',
      on: false,
      data: '01'
    },
    {
      id: 1,
      name: "煤气探测",
      type: '0504',
      on: false,
      data: '01'
    },
    {
      id: 1,
      name: "烟感探测",
      type: '0505',
      on: false,
      data: '01'
    },
    {
      id: 1,
      name: "门磁探测",
      type: '0506',
      on: false,
      data: '01'
    },
    ]
    this.setData({
      newlist: list
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

    this.updata();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.updata()
    wx.stopPullDownRefresh({
      complete: (res) => { },
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.updata()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  more: function (e) {
    var show = this.data.moreshow
    if (show == "more-show") {
      show = "more-hidden"
    } else {
      show = "more-show"
    }
    this.setData({
      moreshow: show
    })
  },
  morebindChange: function (e) {
    this.setData({
      currentTab: e.target.dataset.current
    });
    this.checkCor();
  },
  //在这里加数据
  bindChange: function (e) {
    var index = e.detail.current;
    var that = this;
    that.setData({
      currentTab: index
    });
    this.checkCor();
  },
  /** 
   * 点击tab切换 
   */
  swichNav: function (e) {
    var that = this;
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      console.log(e.target.dataset.current);
      that.setData({
        currentTab: e.target.dataset.current
      })
    }
  },
  checkCor: function () {
    if (this.data.currentTab > 4) {
      this.setData({
        scrollleft: 300
      })
    } else {
      this.setData({
        scrollleft: 0
      })
    }
  },

  edit: function (e) {
    var that = this
    var dev = e.currentTarget.dataset.dev;
    var isAction = this.data.action;
    if (isAction) {
      var pages = getCurrentPages(); //当前页面
      var prevPage = pages[pages.length - 2]; //上一页面
      prevPage.devCallBack(dev);
      wx.navigateBack()
    } else {
      wx.showActionSheet({
        itemList: ['编辑设备', '分享设备', '删除设备'],
        success: function (res) {

          if (res.tapIndex == 0) {
            wx.navigateTo({
              url: '../edit/edit?id=' + dev.id,
            })
          }
          if (res.tapIndex == 1) {
            wx.navigateTo({
              url: '../../share/share?type=device&id=' + dev.did + "&name=" + dev.name,
            })
          }

          if (res.tapIndex == 2) {
            wx.showModal({
              title: "删除设备",
              success: function (res) {
                if (res.cancel) {
                  //点击取消,默认隐藏弹框
                } else {
                  that.del(dev.id)
                }
              },
            })
          }
        }
      })
    }
  },
  share: function (id, name) {
    wx.navigateTo({
      url: '../../share/share?id=' + id + "&name=" + name,
    })
  },
  del: function (id) {
   
  }
})