var smh = require("../../../utils/smh.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    xinghao: 9,
    yingxiao: 21,
    study: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.get(options.id)
  },
  study: function () {
    var st = this.data.study;
    if (!st) {
      wx.showToast({
        title: '进入红外学习',
      })
    } else {
      wx.showToast({
        title: '退出红外学习',
      })
    }
    this.setData({
      study: !st
    })
  },
  get: function (id) {
    var that = this
    smh.get("device?id=" + id, {
      success: function (res) {
        wx.setNavigationBarTitle({
          title: res.data.displayName,
        })
        that.setData({
          device: res.data
        })
      },
      fail: function (res) {
        wx.showToast({
          title: res.msg,
          icon: "loading"
        })
      }
    })
  },
  xinghao: function () {
    var that = this;
    wx.showActionSheet({
      itemList: ["1", "2", "3", "4", "5"],
      success: function (res) {
        var data = res.tapIndex + 7
        var e = {
          currentTarget: {
            dataset: {
              mode: data
            }
          }
        }
        that.send(e);
      }
    });
  },
  yingxiao: function () {
      var that = this;
      wx.showActionSheet({
        itemList: ["1", "2", "3", "4"],
        success: function (res) {
          var data = res.tapIndex + 20
          var e = {
            currentTarget: {
              dataset: {
                mode: data
              }
            }
          }
          that.send(e);
        }
      });
  },
  send: function (e) {
    var mode = parseInt(e.currentTarget.dataset.mode)
    var data = ""
    if (mode < 16) {
      data = "0" + mode.toString(16).toUpperCase()
    } else
      data = mode.toString(16).toUpperCase();

    var code = "14"
    var star = 3
    if (this.data.study) {
      code = "11"
      star = 0
    }
    var that = this;
    smh.opt(that.data.device.id, code, star, data, {
      success: function (res) {

      },
      fail: function (res) {
        wx.showToast({
          title: res.msg,
          icon: "loading"
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})