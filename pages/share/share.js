// miniprogram/pages/area/share/share.js
import drawQrcode from '../../utils/weapp-qrcode.js';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: 0,
    name: "",
    role: 0,
    expireTime: "2020/12/12",
    time: "00:00",
    typeindex: 0,
    types: ["普通用户", "高级用户"],
    timeindex: 0,
    quxiao: false,
    times: ["临时分享", "永久分享", "取消分享"]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    var id = options.id;
    var type = options.type;
    var name = options.name;
    this.setData({
      type,
      id: id,
      name: name
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  bindDateChange: function (e) {
    this.setData({
      expireTime: e.detail.value,
    })
  },
  bindTimeChange: function (e) {
    this.setData({
      time: e.detail.value,
    })
  },
  bindChange: function (e) {
    var index = e.detail.value;
    this.setData({
      quxiao: index == 2,
      timeindex: index,
    })
  },
  bindtypeChange: function (e) {
    var index = e.detail.value;
    this.setData({
      typeindex: index,
    })
  },

  share: function () {
    var key = this.data.id;
    var type = parseInt(this.data.timeindex) + 1;
    let size = {}
    size.w = wx.getSystemInfoSync().windowWidth / 750 * 450
    size.h = size.w
    var that = this
    var share = "http://cdyong.cn?id=" + that.data.id + "&expireTime=" + that.data.expireTime + " " + that.data.time;
    this.setData({
      youxiaoqi: true
    })
    drawQrcode({
      width: size.w,
      height: size.h,
      canvasId: 'myQrcode',
      // ctx: wx.createCanvasContext('myQrcode'),
      text: share
      // v1.0.0+版本支持在二维码上绘制图片
    })
  },
  submitForm: function (e) {
    var type = this.data.timeindex;
    if (type == 2) {
      this.quxiao()
    } else {
      this.share();
    }
  }
})