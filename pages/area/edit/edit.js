
Page({

  /**
   * 页面的初始数据
   */
  data: {
    area: {
      id: 0,
      name: '会客区',
      img: "icon-woshi",
      index: 0
    },
    iconfontlist: [
      "icon-webiconwoshi",
      "icon-woshi",
      "icon-kongjian09",
      "icon-shufang",
      "icon-chashi",
      "icon-keting",
      "icon-chufang",
      "icon-canting",
      "icon-batai",
      "icon-dianying",
      "icon-jianshen",
      "icon-yule",
      "icon-bangongshi",
      "icon-yangtai",
      "icon-damen",
      "icon-hekriconqingjingzoulang"
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {},
  changeIcon: function (e) {
    var index = e.currentTarget.dataset.index;
    var area = this.data.area;
    area.img = this.data.iconfontlist[index];
    this.setData({
      area: area
    })
  },
  InputChange: function (e) {
    var area = this.data.area;
    area.name = e.detail.value;
    this.setData({
      area: area
    })
  },
  Index: function (e) {
    var area = this.data.area;
    if (area.index > 0) {
      area.index = 0
    } else {
      area.index = 1
    }

    this.setData({
      area: area
    })
  },
  submitForm: function () {
    var area = this.data.area;
    if (area == null || area.name == null || area.name == "" || area.img == null || area.img == "") {
      wx.showModal({
        title: "请完善资料",
      })
      return;
    }
    wx.navigateBack({
      complete: (res) => {},
    })
  }
})