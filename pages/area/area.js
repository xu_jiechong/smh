
Page({

  /**
   * 页面的初始数据
   */
  data: {
    modalhid: true,
    slider: 0,
    pageSize: 20,
    //区域id
    id: 0,
    //区域名称
    name: "",
    touch: 0,
    scenes: [

    ],
    devices: []
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var id = options.id;
    var name = options.name;
    wx.setNavigationBarTitle({
      title: name,
    })

    this.setData({
      id: id,
      name: name
    })
    this.devlist();
    this.sclist();
  },
  devlist: function () {
    var that = this;
    var list = [{
        id: 1,
        name: "开关",
        type: '0101',
        on: true,
        data: '01'
      },
      {
        id: 2,
        name: "调光",
        type: '0102',
        on: true,
        data: '51'
      },
      {
        id: 3,
        name: "插座",
        type: '0103',
        on: true,
        data: '01'
      },
      {
        id: 4,
        name: "窗帘",
        type: '0201',
        on: false,
        data: '01'
      }, 
      {
        id: 6,
        name: "空调",
        type: '0401',
        on: true,
        data: '01'
      },
      {
        id: 6,
        name: "电视",
        type: '0402',
        on: false,
        data: '01'
      },
      {
        id: 6,
        name: "背景音乐",
        type: '0405',
        on: false,
        data: '01'
      },
      {
        id: 1,
        name: "电表",
        type: '0301',
        on: true,
        data: '01'
      },
      {
        id: 1,
        name: "红外探测",
        type: '0503',
        on: false,
        data: '01'
      },
      {
        id: 1,
        name: "煤气探测",
        type: '0504',
        on: false,
        data: '01'
      },
      {
        id: 1,
        name: "烟感探测",
        type: '0505',
        on: false,
        data: '01'
      },
      {
        id: 1,
        name: "门磁探测",
        type: '0506',
        on: false,
        data: '01'
      },
    ]
    this.setData({
      devices: list
    })
  },
  sclist: function () {
    var list = [{
        id: 1,
        name: "回家模式",
        data: "17",
        img: "icon-return-home.png",
      },
      {
        id: 2,
        name: "离开模式",
        data: "17",
        img: "tc.png",
      },
      {
        id: 3,
        name: "健身模式",
        data: "17",
        img: "icon-jianshen.png",
      },
      {
        id: 4,
        name: "起夜模式",
        data: "17",
        img: "qiye.png",
      }
    ]
    this.setData({
      scenes: list
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    if (this.data.id > 0) {
      this.devlist();
      this.sclist();
    }
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.id > 0) {
      this.devlist();
      this.sclist();
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  scopt: function (e) {
    var id = e.currentTarget.dataset.id;
    var data = e.currentTarget.dataset.data;
    var code = '19'
    wx.showToast({
      title: '情景启动',
    })
  },
  open0202: function (e) {
    var index = e
    var dev = this.data.devices[index];
    console.log(dev);
    var setdata = "devices[" + index + "].on";
    var that = this;
    that.setData({
      [setdata]: true
    });
    wx.showToast({
      title: '',
    })
  },
  stop0202: function (e) {
    wx.showToast({
      title: '',
    })
  },
  close0202: function (e) {
    var index = e;
    var dev = this.data.devices[index];
    var that = this;
    var setdata = "devices[" + index + "].on";

    that.setData({
      [setdata]: false
    });
    wx.showToast({
      title: '',
    })
  },
  touchstart: function (e) {
    this.touch = e.timeStamp;
  },
  touchend: function (e) {
    this.touch = e.timeStamp - this.touch

  },
  touchmove: function (e) {
    console.log(e)
  },
  tiaoguang: function (e) {
    var index = e.currentTarget.dataset.index;
    this.opt0102(index);
  },
  modalconfirm: function () {
    this.setData({
      modalhid: true
    })
  },
  changeSlider(e) {
    console.log(e.detail.value)
    this.setData({
      slider: e.detail.value
    })
    var index = this.data.currentdevIndex;
    var dev = this.data.devices[index];
    var setdata = "devices[" + index + "].on";
    var setdata = "devices[" + index + "].data";
    var that = this;
    var on = true;
    var data = parseInt(e.detail.value) + 10;
    var data = '';
    if (data <= 15)
      data = "0" + data.toString(16);
    else
      data = data.toString(16);

    if (data == "0a") data = "00";
    wx.showToast({
      title: '',
    })
    that.setData({
      [setdata]: on,
      [setdata]: data
    });
  },
  opt0102: function (index) {
    var dev = this.data.devices[index];
    var data = parseInt(dev.data, 16)

    data = data - 10;
    if (data < 0) data = 0;

    this.setData({
      slider: data,
      modalhid: false,
      currentdevIndex: index
    })
  },
  deviceClick: function (e) {
    var index = e.currentTarget.dataset.index;
    var that = this;
    var dev = this.data.devices[index];
    if (dev.type == '0201' || dev.type == '0202') {
      wx.showActionSheet({
        itemList: ['打开', '关闭', '停止'],
        success: res => {
          if (res.tapIndex == 0) {
            this.open0202(index)
          }
          if (res.tapIndex == 1) {
            this.close0202(index)
          }
          if (res.tapIndex == 2) {
            this.stop0202(index)
          }
        }
      })
      return;
    }
    if (dev.type == '0102' && that.touch > 350) {
      this.opt0102(index);
      return;
    }
    if (dev.type == "0101" || dev.type == '0102'| dev.type == '0103') {
      var setdata = "devices[" + index + "].on";   
      wx.showToast({
        title: '',
      })
      that.setData({
        [setdata]: !dev.on,
      });
      return;
    }
    if (parseInt(dev.type, 16) > 0x0500 & parseInt(dev.type, 16) < 0x05ff) {
      wx.navigateTo({
        url: '../safe/safe',
      })
      return;
    }
    wx.navigateTo({
      url: '../device/' + dev.type + "/" + dev.type + "?id=" + dev.id,
    })
  }
})