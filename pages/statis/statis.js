// miniprogram/pages/statis/statis.js
const app = getApp();
import * as echarts from '../../utils/ec-canvas/echarts';
// miniprogram/pages/device/0104/0104.js

Page({

  /**
   * 页面的初始数据
   */
  data: {
    ec: {
      lazyLoad: true
    },
    currentTab: 0,
    currentTab: 0,
    winWidth: 0,
    winHeight: 0,
    scrollleft: 0,
    start: "",
    end: "",
    max: '',
    pageSize: 31,
    list: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var date = new Date();
    var end = this.getTime(date)
    date.setMonth(date.getMonth() - 1);
    var start = this.getTime(date);

    this.setData({
      start: start,
      end: end
    })

    let that = this;
    /** 
     * 获取系统信息 
     */
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight
        });
      }
    });
    //echart
    this.ecComponent = this.selectComponent('#mychart-dom-line');

    var id = options.id
    var max = options.max
    var name=options.name

    
    this.setData({
      id: id,
      max: max
    })
    this.list();

  },

  list: function () {
    this.setData({
      list:[{
        time:'2020/8/8',
        degree:38
      },{
        time:'2020/8/7',
        degree:34
      },{
        time:'2020/8/6',
        degree:40
      },{
        time:'2020/8/5',
        degree:45
      },{
        time:'2020/8/4',
        degree:38
      },{
        time:'2020/8/3',
        degree:37
      },{
        time:'2020/8/2',
        degree:36
      }]
    })

    //this.echartinit(this.data.list.reverse())
  },
  setOption: function (chart, times, datas) {
    var option = {
      xAxis: {
        type: 'category',
        boundaryGap: false,
        //日期
        data: times
      },
      yAxis: {
        type: 'value',
        boundaryGap: [0, '50%']
      },
      tooltip: {
        formatter: '{b}\n{c}'
      },
      series: [{
        name: '电量',
        type: 'line',
        smooth: true,
        symbol: 'none',
        sampling: 'average',
        itemStyle: {
          color: 'rgb(255, 255, 255)'
        },
        areaStyle: {
          color: 'rgb(7, 193, 96)'
        },
        //数值
        data: datas
      }]
    };
    chart.setOption(option);
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.list();
    wx.stopPullDownRefresh()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.list();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  getTime: function (date) {
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hour = date.getHours();
    var minute = date.getMinutes();
    var second = date.getSeconds();

    var formatNumber = function (n) {
      var s = n.toString();
      return s[1] ? s : '0' + s;
    };
    return [year, month, day].map(formatNumber).join('/')
  },
  bindstartDateChange: function (e) {
    this.setData({
      start: e.detail.value
    })
    this.list();
  },
  bindendDateChange: function (e) {
    this.setData({
      end: e.detail.value
    })
    this.list();
  },
  bindChange: function (e) {
    var index = e.detail.current;
    var that = this;
    that.setData({
      currentTab: index
    });
    this.checkCor();
  },
  /** 
   * 点击tab切换 
   */
  swichNav: function (e) {
    var that = this;
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {

      that.setData({
        currentTab: e.target.dataset.current
      })
    }
  },
  checkCor: function () {
    if (this.data.currentTab > 4) {
      this.setData({
        scrollleft: 300
      })
    } else {
      this.setData({
        scrollleft: 0
      })
    }
  },
  echartinit: function (list) {
    this.ecComponent.init((canvas, width, height, dpr) => {
      // 获取组件的 canvas、width、height 后的回调函数
      // 在这里初始化图表
      const chart = echarts.init(canvas, null, {
        width: width,
        height: height,
        devicePixelRatio: dpr // new
      });
      var times = []
      var datas = []
      for (let index = 0; index < list.length; index++) {
         var element = list[index];
        var time=element.time.substring(5,10)
        times.push(time)
        datas.push(element.degree)
      }

      this.setOption(chart, times, datas);

      // 将图表实例绑定到 this 上，可以在其他成员函数（如 dispose）中访问
      this.chart = chart;
      // 注意这里一定要返回 chart 实例，否则会影响事件处理等
      return chart;
    });
  }
})