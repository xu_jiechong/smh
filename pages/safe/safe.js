
Page({

  /**
   * 页面的初始数据
   */
  data: {
    level: ["未设置", "无音告警", "面板告警", "一般告警", "重要告警", "严重告警"],
    model: ["撤防", "设防", "屏蔽", "停用"],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.list();
  },


  list: function () {
    var that = this
    var list = [{
        id: 1,
        name: "红外探测",
        type: '0801',
        on: false,
        level: 1,
        model: 0,
        enable: true,
        data: '01'
      },
      {
        id: 1,
        name: "煤气探测",
        type: '0802',
        on: false,
        level: 1,
        model: 1,
        enable: true,
        data: '01'
      },
      {
        id: 1,
        name: "烟感探测",
        type: '0803',
        on: false,
        level: 2,
        model: 2,
        enable: true,
        data: '01'
      },
      {
        id: 1,
        name: "门磁探测",
        type: '0803',
        on: false,
        level: 3,
        model: 3,
        enable: false,
        data: '01'
      }
    ]
    this.setData({
      list: list
    })
  },
  switchChange: function (e) {
    var that = this;
    var enable = false;
    var check = e.detail.value;
    if (check) {
      enable = true
    }
    var index = e.target.dataset.index;
    var dev = this.data.list[index];

    var data = enable ? 120 : 121
    var setdata = "list[" + index + "].model";
    that.setData({
      [setdata]: enable ? 1 : 0
    })
    wx.showToast({
      title: "设置成功",
    })
  },
  setSafe: function (e) {
    wx.showToast({
      title: "设置成功",
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

    this.list();
    wx.stopPullDownRefresh({
      complete: (res) => {},
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

    this.list();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})