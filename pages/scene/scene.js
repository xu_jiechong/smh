
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageSize: 20,
    currentTab: 0,
    winWidth: 0,
    winHeight: 0,
    scrollleft: 0,
    moreshow: "more-hidden",
    seceneall: [],
    areaList: [],
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    /** 
     * 获取系统信息 
     */
    var that = this
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight
        });
      }
    });
    this.all();
    this.list();

  },
  list: function () {
    var list = [{
        id: 1,
        name: "客厅",
        secenes: [{
            id: 1,
            areaName: '客厅',
            name: "回家模式",
            data: "17",
            img: "icon-return-home.png",
          },
          {
            id: 2,
            areaName: '客厅',
            name: "离开模式",
            data: "17",
            img: "tc.png",
          }
        ]
      },
      {
        id: 1,
        name: "健身房",
        secenes: [{
          id: 3,
          areaName: '健身房',
          name: "健身模式",
          data: "17",
          img: "icon-jianshen.png",
        }]
      }, {
        id: 1,
        name: "卧室",
        secenes: [{
          id: 4,
          areaName: '卧室',
          name: "起夜模式",
          data: "17",
          img: "qiye.png",
        }]
      },
      {
        id: 1,
        name: "楼顶花园",
        secenes: [{
          id: 4,
          areaName: '楼顶花园',
          name: "喷灌",
          data: "17",
          img: "icon-jieneng.png",
        }]
      }
    ]
    this.setData({
      areaList: list
    })
  },
  all: function () {
    var list = [{
        id: 1,
        areaName: '客厅',
        name: "回家模式",
        data: "17",
        img: "icon-return-home.png",
      },
      {
        id: 2,
        areaName: '客厅',
        name: "离开模式",
        data: "17",
        img: "tc.png",
      },
      {
        id: 3,
        areaName: '健身房',
        name: "健身模式",
        data: "17",
        img: "icon-jianshen.png",
      },
      {
        id: 4,
        areaName: '卧室',
        name: "起夜模式",
        data: "17",
        img: "qiye.png",
      },
      {
        id: 4,
        areaName: '楼顶花园',
        name: "喷灌",
        data: "17",
        img: "icon-jieneng.png",
      }
    ]

    this.setData({
      seceneall: list
    })
  },
  areaSclist: function () {
    //not
  },
  opt: function (e) {
    var id = e.currentTarget.dataset.id;
    wx.showToast({
      title: "场景启动",
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {


    wx.stopPullDownRefresh();

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  more: function (e) {
    var show = this.data.moreshow
    if (show == "more-show") {
      show = "more-hidden"
    } else {
      show = "more-show"
    }
    this.setData({
      moreshow: show
    })
  },
  morebindChange: function (e) {
    this.setData({
      currentTab: e.target.dataset.current
    });
    this.checkCor();
  },
  //在这里加数据
  bindChange: function (e) {
    var index = e.detail.current;
    var that = this;
    that.setData({
      currentTab: index
    });
    this.checkCor();
  },
  /** 
   * 点击tab切换 
   */
  swichNav: function (e) {
    var that = this;
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      console.log(e.target.dataset.current);
      that.setData({
        currentTab: e.target.dataset.current
      })
    }
  },
  checkCor: function () {
    if (this.data.currentTab > 4) {
      this.setData({
        scrollleft: 300
      })
    } else {
      this.setData({
        scrollleft: 0
      })
    }
    this.areaSclist();
  },
  edit: function (e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: './edit/edit?id=' + id,
    })
  }
})