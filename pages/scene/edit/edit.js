// pages/scene/edit/edit.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    scene: {
      name:'回家模式',
      areaName:'客厅',
      img:"icon-return-home.png"
    },
    areaList: [],
    iconfontlist: [
      "icon-return-home",
      "icon-dianyuan",
      "icon-jieneng",
      "icon-lijiamoshi",
      "icon-huiyi",
      "icon-qingjingmoshiyuedu",
      "icon-chenqi",
      "icon-yule",
      "icon-dianying",
      "icon-qingjingmoshishuimian",
      "icon-batai",
      "icon-shefang",
      "icon-cefang",
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var id = options.id;
    this.areaList()
  },
  //区域列表
  areaList: function () {
    var gate = wx.getStorageSync('gate')
    var that = this;
    var list = [{
      id: 1,
      name: "客厅",
      secenes: [{
          id: 1,
          areaName: '客厅',
          name: "回家模式",
          data: "17",
          img: "icon-return-home.png",
        },
        {
          id: 2,
          areaName: '客厅',
          name: "离开模式",
          data: "17",
          img: "tc.png",
        }
      ]
    },
    {
      id: 1,
      name: "健身房",
      secenes: [{
        id: 3,
        areaName: '健身房',
        name: "健身模式",
        data: "17",
        img: "icon-jianshen.png",
      }]
    }, {
      id: 1,
      name: "卧室",
      secenes: [{
        id: 4,
        areaName: '卧室',
        name: "起夜模式",
        data: "17",
        img: "qiye.png",
      }]
    },
    {
      id: 1,
      name: "楼顶花园",
      secenes: [{
        id: 4,
        areaName: '楼顶花园',
        name: "喷灌",
        data: "17",
        img: "icon-jieneng.png",
      }]
    }
  ]
  this.setData({
    areaList: list
  })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  InputChange:function(e){
    var sc=this.data.scene;
    sc.name=e.detail.value;
    this.setData({
      scene:sc
    })
  },
  action: function () {
    wx.navigateTo({
      url: '../action/action?id=' + this.data.scene.id,
    })
  },
  changeIcon: function (e) {
    console.log(e)
    var index = e.currentTarget.dataset.index;
    var scene = this.data.scene;
    if (scene == null) scene = {};
    scene.img = this.data.iconfontlist[index];
    this.setData({
      scene: scene
    })
  },
  bindAareaChange: function (e) {
   
    var area = this.data.areaList[e.detail.value]
    console.log(area);
    var sc = this.data.scene
    sc.areaKey = area.areaKey
    sc.areaName = area.name
    this.setData({
      scene: sc
    })
  },
  index: function () {
    var sc = this.data.scene;
    if (sc.index > 0) {
      sc.index = 0;
    } else {
      sc.index = 1;
    }
    this.setData({
      scene: sc
    })
  },
  submitForm: function (e) {
    wx.navigateBack({
      complete: (res) => {},
    })
  }
})