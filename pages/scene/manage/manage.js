
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageSize: 2000,
    currentTab: 0,
    winWidth: 0,
    winHeight: 0,
    scrollleft: 0,
    moreshow: "more-hidden",
    seceneall: [],
    areaList: [],
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    /** 
     * 获取系统信息 
     */
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight
        });
      }
    });
    this.list();
  },
  updata:function(){
     if(this.data.currentTab==0){
       this.all()
     } 
     else{
       this.areaSclist()
     }
  },
  list: function () {
    var gate = wx.getStorageSync('gate')
    var that = this;
    smh.get('area/list?mac=' + gate.mac + "&pageStart=0" + "&pageSize=2000", {
      success: function (res) {
        that.setData({
          pageSize:that.data.pageSize+20,
          areaList: res.data
        })
      },
      fail: function (res) {
        wx.showToast({
          title: res.msg,
          icon: 'loading'
        })
      }
    })
  },
  all: function () {
    var gate = wx.getStorageSync('gate')
    var that = this;
    smh.get('Scene/newlist?mac=' + gate.mac + "&pageStart=0" + "&pageSize=" + this.data.pageSize, {
      success: function (res) {
        that.setData({
          seceneall: res.data,
          pageSize:that.data.pageSize+20
        })
      },
      fail: function (res) {
        wx.showToast({
          title: res.msg,
          icon: 'loading'
        })
      }
    })
  },
  areaSclist: function () {
    var that = this;
    var index = this.data.currentTab;
    if (index == 0) return;
    index = index - 1;
    var area=that.data.areaList[index]; 
    var setdata = "areaList[" + index + "].secenes";
    smh.get('area/scenes?id=' +area.id + "&pageStart=0" + "&pageSize=" + this.data.pageSize, {
      success: function (res) {
        that.setData({
          [setdata]: res.data,
          pageSize:that.data.pageSize+20
        });
      },
      fail: function (res) {
        wx.showToast({
          title: res.msg,
          icon: 'loading'
        })
      }
    })
  },
  opt:function(e){
    var id = e.currentTarget.dataset.id;
    smh.get("Scene/opt?id="+id,{
      success:function(re){
        wx.showToast({
          title: "场景启动",     
        })
      },
      fail:res=>{
        wx.showToast({
          title: res.msg,
          icon:"loading"
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.updata()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
    this.updata()
    wx.stopPullDownRefresh();

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    console.log('onReachBottom');
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  more: function (e) {
    var show = this.data.moreshow
    if (show == "more-show") {
      show = "more-hidden"
    } else {
      show = "more-show"
    }
    this.setData({
      moreshow: show
    })
  },
  morebindChange: function (e) {
    this.setData({
      currentTab: e.target.dataset.current
    });
    this.checkCor();
  },
  //在这里加数据
  bindChange: function (e) {
    var index = e.detail.current;
    var that = this;
    that.setData({
      currentTab: index
    });
    this.checkCor();
  },
  /** 
   * 点击tab切换 
   */
  swichNav: function (e) {
    var that = this;
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      console.log(e.target.dataset.current);
      that.setData({
        currentTab: e.target.dataset.current
      })
    }
  },
  checkCor: function () {
    if (this.data.currentTab > 4) {
      this.setData({
        scrollleft: 300
      })
    } else {
      this.setData({
        scrollleft: 0
      })
    }
    this.areaSclist();
  },
  edit: function (e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../edit/edit?id='+id,
    })
  }
})