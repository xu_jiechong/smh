
Page({

  /**
   * 页面的初始数据
   */
  data: {
    kg: ["打开", "关闭"],
    cl: ["打开", "关闭", "暂停"],
    kt: ["打开", "关闭", "制冷", "制热", "抽湿", "送风"],
    list: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      id: options.id
    })
  },
  get: function () {
    var id = this.data.id

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  kgbindChange: function (e) {
    var index = e.detail.value;
    var key = this.data.kg[index];
    var aindex = e.currentTarget.dataset.index;
    var action = this.data.list[aindex];
    console.log(action)
    action.Enable = true;
    action.Code = "14";
    action.Start = 4;


    if (key == "打开") {

      action.Ation = "01"
      action.action = "打开"
    } else {
      action.Ation = "00"
      action.action = "关闭"
    }
    var setdata = "list[" + aindex + "]";
    this.setData({
      [setdata]: action
    })
    //TODO 向服务器更新
  },
  ktbindChange: function (e) {
    var index = e.detail.value;
    var key = this.data.kt[index];
    var aindex = e.currentTarget.dataset.index;
    var action = this.data.list[aindex];
    console.log(key)
    action.Enable = true;
    action.Code = "14";
    action.Start = 4;

    switch (key) {
      case '关闭':
        action.action = "关闭";
        action.Ation = "00";
        break;
      case '制冷':
        action.action = "制冷"
        action.Ation = "01";
        break;
      case '制热':
        action.action = "制热"
        action.Ation = "02";
        break;
      case '抽湿':
        action.action = "抽湿"
        action.Ation = "03";
        break;
      case '送风':
        action.action = "送风"
        action.Ation = "04";
        break;
      case '自动':
        action.action = "自动"
        action.Ation = "05";
        break;
      case '打开':
        action.action = "打开"
        action.Ation = "07";
        break;
    }
    var setdata = "list[" + aindex + "]";
    this.setData({
      [setdata]: action
    })
    //TODO 向服务器更新
  },
  del: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    wx.showModal({
      title: "删除",
      success: function (res) {
        if (res.cancel) {
          //点击取消,默认隐藏弹框
        } else {
          that.data.list.splice(index, 1)
          var list = that.data.list;
          that.setData({
            list: list
          })
        }
      },
    })
  },
  add: function () {
    wx.navigateTo({
      url: '../../device/manage/manage?action=true',
    })
  }
})