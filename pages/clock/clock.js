
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: []
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  list: function () {
     var list=[
       {
         name:"下班回家",
         hour:'18',
         minute:'00',
         week1:true,
         week2:true,
         week3:true,
         week4:true,
         week5:true,
         week6:false,
         week0:false,
         enable:true
       },
       {
        name:"上班",
        hour:'08',
        minute:'00',
        week1:true,
        week2:true,
        week3:true,
        week4:true,
        week5:true,
        week6:false,
        week0:false,
        enable:true
      }
     ]
     this.setData({
       list
     })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.list()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var size = this.data.PageSize + 20;
    this.setData({
      PageSize: size
    })
    this.list();
    wx.stopPullDownRefresh({
      complete: (res) => {},
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var size = this.data.PageSize + 20;
    this.setData({
      PageSize: size
    })
    this.list();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  switchChange: function (e) {
    var that = this;
    var enable = false;
    var check = e.detail.value;
    if (check) {
      enable = true
    }
    var index = e.target.dataset.index;
    var ug = this.data.list[index];
    var setdata = "list[" + index + "].enable";
    that.setData({
      setdata: enable
    })
    wx.showToast({
      title: '设置成功',
    })
  },
  add: function (e) {

    wx.navigateTo({
      url: './edit/edit?id=0',
    })
  },
  edit: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var id = this.data.list[index].id
    wx.showActionSheet({
      itemList: ['编辑', '删除'],
      success: function (res) {
        if (res.tapIndex == 0) {
          wx.navigateTo({
            url: './edit/edit?id=' + id,
          })
        } else {
          that.del(index);
        }
      }
    })
  },
  del: function (index) {
    var that = this;
    var clock=this.data.list[index]
    wx.showModal({
      title: "删除",
      success: function (res) {
        if (res.cancel) {
          //点击取消,默认隐藏弹框
        } else {
          that.data.list.splice(index, 1)
          var list = that.data.list;
          that.setData({
            list: list
          })
        }
      },
    })
  },
})