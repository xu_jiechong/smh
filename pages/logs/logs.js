Page({

  /**
   * 页面的初始数据
   */
  data: {
    level: ["未设置", "无音告警", "面板告警", "一般告警", "重要告警", "严重告警"],
    nodata: true,
    list: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.list()
  },
  list: function () {
    var list = [{
      time: '2020/8/11 17:19',
      level: 4,
      deal:false,
      message: '检测到煤气报警'
    },
    {
      time: '2020/8/11 17:19',
      level: 4,
      deal:false,
      message: '检测到烟雾报警'
    },{
      time: '2020/8/11 17:19',
      level: 4,
      deal:false,
      message: '检测到红外报警'
    }]

    this.setData({
      list
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.list();
    wx.stopPullDownRefresh({
      complete: (res) => {},
    })

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.list();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  read: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var log = this.data.list[index];

    var setdata = "list[" + index + "].deal";
    wx.showModal({
      content: log.message,
      success: function (res) {
        if (res.cancel) {} else {
          that.setData({
            [setdata]: true
          });
        }
      }
    })
  }
})