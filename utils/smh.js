var svr = "http://localhost:61420/";
function success(res, callback) {
  var data = res.data;
  if (data.code == 'ok' && res.statusCode == 200) {
    callback.success(data);
  } else {
    if(data.code=='403'){
      wx.setStorageSync('token', "")
      wx.navigateTo({
        url: "/pages/login/login",
      })
      return;
    }
    callback.fail(res.data)
  }
}
function fail(res, callback) {
  console.log(res)
  callback.fail({
    msg: "请求失败"
  })
}
function get(url, callback) {
  var token = wx.getStorageSync('token');
  console.log(svr + url);
  if (callback == undefined || typeof callback != 'object') {

    callback = {
      success: function () {},
      fail: function () {}
    }
  }
  if (callback.success == undefined || typeof callback.success != "function") {

    callback.success = function () {}
  }
  if (callback.fail == undefined || typeof callback.fail != "function") {
    callback.fail = function () {}
  }
  wx.request({
    url: svr + url,
    method: 'get',
    header: {
      "Content-Type": "application/json",
      "Authorization": token
    },
    success: function (res) {
      success(res, callback)
    },
    fail: function (res) {
      fail(res, callback)
    }
  })
};

function post(url, data, callback) {
  var that = this;
  var token = wx.getStorageSync('token');
  console.log(svr + url);
  //console.log(data);
  if (callback == undefined || typeof callback != 'object') {
    callback = {
      success: function () {},
      fail: function () {}
    }
  }
  if (callback.success == undefined || typeof callback.success != "function") {
    callback.success = function () {}
  }
  if (callback.fail == undefined || typeof callback.fail != "function") {
    callback.fail = function () {}
  }

  wx.request({
    url: svr + url,
    data:data ,
    method: 'post',
    header: {
      "content-type": "application/json",
      "Authorization": token
    },
    success: function (res) {
      success(res, callback)
    },
    fail: function (res) {
      console.log(res)
      fail(res, callback)
    }
  })
};
function del(url, callback) {
  var token = wx.getStorageSync('token');
  console.log(svr + url);
  if (callback == undefined || typeof callback != 'object') {

    callback = {
      success: function () {},
      fail: function () {}
    }
  }
  if (callback.success == undefined || typeof callback.success != "function") {

    callback.success = function () {}
  }
  if (callback.fail == undefined || typeof callback.fail != "function") {
    callback.fail = function () {}
  }
  wx.request({
    url: svr + url,
    method: 'delete',
    header: {
      "Content-Type": "application/json",
      "Authorization": token
    },
    success: function (res) {
      success(res, callback)
    },
    fail: function (res) {
      fail(res, callback)
    }
  })
};

function put(url, data, callback) {
  var that = this;
  var token = wx.getStorageSync('token');
  console.log(svr + url);
 
  if (callback == undefined || typeof callback != 'object') {
    callback = {
      success: function () {},
      fail: function () {}
    }
  }
  if (callback.success == undefined || typeof callback.success != "function") {
    callback.success = function () {}
  }
  if (callback.fail == undefined || typeof callback.fail != "function") {
    callback.fail = function () {}
  }
  wx.request({
    url: svr + url,
    data: data,
    method: 'put',
    header: {
      "Content-Type": "application/json",
      "Authorization": token
    },
    success: function (res) {
      success(res, callback)
    },
    fail: function (res) {
      fail(res, callback)
    }
  })
};
function opt(id,code,start,data,callback){
  get('device/opt?id='+id+"&code="+code+"&start="+start+"&data="+data,callback);
}
function toHex(str) {　　　　
	var val = [];　　
	if(str == null) str = "FFFFFFFFFFFFFFFF";
	if(str.length < 16)
		str = str + "FFFFFFFFFFFFFFFF".substr(0, 16 - str.length);
	for(var i = 0; i < str.length; i = i + 2) {　　　　　　
		var data = (str[i] + str[i + 1]).toString(16);
		val.push(data);
	}　
	return val;　　
}
module.exports = {
  get: get,
  post: post,
  del:del,
  put:put,
  opt:opt,
  toHex:toHex
}